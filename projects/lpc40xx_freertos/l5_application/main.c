#include <stdio.h>

#include "FreeRTOS.h"
#include "board_io.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "ssp2_lab.h"
#include "task.h"

#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"
#include "task.h"
#include <adc.h>
#include <delay.h>
#include <gpio.h>
//#include <gpio_isr.h>
#include <lpc_peripherals.h>
#include <queue.h>

//#define SPI_PART_0_AND_1

//#define SPI_PART_2_WITH_MUTEX
#define SPI_PART_3_EXTRA_CREDIT

#ifdef SPI_PART_0_AND_1

void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 12;
  ssp2__init(spi_clock_mhz);
  ssp2_pin_functions();
  while (1) {
    adesto_flash_id_s id = adesto_read_signature();
    // TODO: printf the members of the 'adesto_flash_id_s' struct
    printf("Manufacture ID = %x ", id.manufacturer_id);
    printf("| Device ID(1) = %x ", id.device_id_1);
    printf("| Device ID(2) = %x ", id.device_id_2);
    printf("| Ext Device ID = %x\n", id.extended_device_id);
    // vTaskDelay(1000);
  }
}

int main(void) {
  // create_blinky_tasks();
  // create_uart_task();

  xTaskCreate(spi_task, "spi_task", (1024U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}

#endif

#ifdef SPI_PART_2_WITH_MUTEX
SemaphoreHandle_t spi_bus_mutex = NULL;
static void spi_id_verification_task(void *p);

void spi_id_verification_task(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      adesto_flash_id_s id = adesto_read_signature();

      if (0x1F != id.manufacturer_id) {
        fprintf(stderr, "Manufacturer ID read failure\n");
        vTaskSuspend(NULL);
        // Kill this task
      }
      xSemaphoreGive(spi_bus_mutex);
    }
  }
}

int main(void) {
  // Initialize SPI, its pins, Adesto flash CS GPIO etc...
  spi_bus_mutex = xSemaphoreCreateMutex();
  const uint32_t spi_clock_mhz = 12;
  ssp2__init(spi_clock_mhz);

  ssp2_pin_functions();
  // Create two tasks that will continously read signature
  xTaskCreate(spi_id_verification_task, "spi_task", (1024U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_id_verification_task, "spi_task", (1024U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
  return 0;
}

#endif

#ifdef SPI_PART_3_EXTRA_CREDIT

SemaphoreHandle_t spi_bus_mutex = NULL;
static void spi_write_page_task(void *p);
static void spi_read_page_task(void *p);
void adesto_read_status_register(void);

void adesto_flash_send_address(uint32_t page_address) {
  const uint8_t dummy_byte = 0xFF;
  ssp2__lab__exchange_byte((page_address >> 16) & dummy_byte); // A23-A16
  ssp2__lab__exchange_byte((page_address >> 8) & dummy_byte);  // A16-A8
  ssp2__lab__exchange_byte((page_address >> 0) & dummy_byte);  // A8-A0
}

static void spi_write_page_task(void *p) {

  const uint32_t page_address = 0x00000000;
  const uint8_t opcode_write_enable = 0x06;
  const uint8_t opcode_to_erase_page = 0x81;
  const uint8_t opcode_to_write_page = 0x02;
  static gpio_s spi_chip_select = {1, 10};

  while (1) {

    if (xSemaphoreTake(spi_bus_mutex, 1000))
     {
      adesto_cs(spi_chip_select);
      ssp2__lab__exchange_byte(opcode_write_enable);
      adesto_ds(spi_chip_select);
      delay__us(2);
      adesto_read_status_register();
      adesto_cs(spi_chip_select);
      ssp2__lab__exchange_byte(opcode_to_erase_page);
      adesto_flash_send_address(page_address);
      adesto_ds(spi_chip_select);
      adesto_read_status_register();

      delay__ms(30);

      adesto_cs(spi_chip_select);
      ssp2__lab__exchange_byte(opcode_write_enable);
      adesto_ds(spi_chip_select);

      adesto_cs(spi_chip_select);
      ssp2__lab__exchange_byte(opcode_to_write_page);
      adesto_flash_send_address(page_address);

      // Sending data
      ssp2__lab__exchange_byte(0x50);
      ssp2__lab__exchange_byte(0xAA);
      ssp2__lab__exchange_byte(0xcc);

      adesto_ds(spi_chip_select);

      adesto_read_status_register(); // Adesto status register
      delay__us(100);
      adesto_read_status_register(); // Adesto status register

      fprintf(stderr, "Data written \n");
      xSemaphoreGive(spi_bus_mutex);
    }
    vTaskDelay(100);
  }
}

static void spi_read_page_task(void *p) {

  const uint32_t page_address = 0x00000000;

  const uint8_t dummy_byte = 0xFF;
  const uint8_t opcode_to_read_page = 0x03;
  adesto_page_bytes_s data = {0};
  static gpio_s spi_chip_select = {1, 10};
  // fprintf(stderr, " read page address is %ld", page_address);
  while (1) {

    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      adesto_cs(spi_chip_select);
      ssp2__lab__exchange_byte(opcode_to_read_page);
      adesto_flash_send_address(page_address);

      data.read_byte_1 = ssp2__lab__exchange_byte(dummy_byte);
      data.read_byte_2 = ssp2__lab__exchange_byte(dummy_byte);
      data.read_byte_3 = ssp2__lab__exchange_byte(dummy_byte);

      adesto_ds(spi_chip_select);

      printf("Received First Byte is  = %x ", data.read_byte_1);
      printf("| Second Byte is = %x ", data.read_byte_2);
      printf("| Third Byte is = %x \n ", data.read_byte_3);
      xSemaphoreGive(spi_bus_mutex);
    }
    vTaskDelay(100);
  }
}
int main(void) {
  const uint32_t spi_clock_mhz = 6;
  ssp2__init(spi_clock_mhz);

  ssp2_pin_functions();
  spi_bus_mutex = xSemaphoreCreateMutex();

  xTaskCreate(spi_write_page_task, "spi page write task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_read_page_task, "spi page read task", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}
#endif
